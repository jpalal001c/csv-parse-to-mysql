#!/usr/bin/env node

//npm install dotenv --save


require('dotenv').config();

var connect = false;
var fs = require('fs');
var parse = require('node-csv-parse');

var argv = require('yargs')
  .usage('truncate table then pour the csv file into a table expenses.\nUsage: $0')
  .default({ t : 'expenses'})
  .example('$0 -f -t ', 'set file set table to pour into')
  .demand('f')
  .alias('f', 'file')
  .describe('f', 'Load a file')
  .argv;

//setup mysql connection
if(connect) {
	var sequelize = new Sequelize(process.env.DBHOST, process.env.DBUSERNAME, process.env.DBPASSWORD, {
	  host: 'localhost',
	  dialect: 'mysql',
	  pool: {
	    max: 5,
	    min: 0,
	    idle: 10000
	  },
	});

    //expense table schema database
var Expense = sequelize.define('expenses', {
  timestamps: false,
  amount: {
    type: Sequelize.STRING
  },
  category: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.TEXT
  },
  created:{
     type:  Sequelize.DATEONLY,
     field: 'expdate'
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});

Expense.sync({force: true}).then(function () { });

}

var parser = parse({delimiter: ','}, function(err, data){
  console.log(data);
});

fs.createReadStream(__dirname+'/' +argv.f).pipe(parser);
